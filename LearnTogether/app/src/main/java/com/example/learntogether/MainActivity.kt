package com.example.learntogether

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.learntogether.ui.theme.LearnTogetherTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LearnTogetherTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    DisplayArticle(
                        article_title = stringResource(R.string.article_title),
                        article_para_i = stringResource(R.string.article_para_i),
                        article_para_ii = stringResource(R.string.article_para_ii)
                    )
                }
            }
        }
    }
}

@Composable
fun DisplayArticle(
    article_title: String,
    article_para_i: String,
    article_para_ii: String,
    modifier: Modifier = Modifier
) {
    val banner = painterResource(R.drawable.bg_compose_background)
    Box(modifier = modifier) {
        Column(
            modifier = modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = banner,
                contentDescription = null,
                contentScale = ContentScale.FillWidth
            )

            Text(
                text = article_title,
                fontSize = 24.sp,
                textAlign = TextAlign.Start,
                modifier = Modifier.padding(
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp,
                    top = 16.dp
                )
            )

            Text(
                text = article_para_i,
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(
                    start = 16.dp,
                    end = 16.dp,
                )
            )

            Text(
                text = article_para_ii,
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp,
                    top = 16.dp
                )
            )
        }
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LearnTogetherTheme {
        DisplayArticle(
            article_title = stringResource(R.string.article_title),
            article_para_i = stringResource(R.string.article_para_i),
            article_para_ii = stringResource(R.string.article_para_ii)
        )
    }
}